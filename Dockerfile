FROM buildpack-deps:bullseye-scm

ENV LANG C.UTF-8

RUN set -x \
    && apt-get update \
    && apt-get install --no-install-recommends -y \
        locales \
    && locale-gen $LANG \
    && rm -rf /var/lib/apt/lists/*

# Install dependencies to run browsers, cypress, and selenium.
RUN set -x \
    && apt-get update \
    && apt-get install --no-install-recommends -y \
        bzip2 \
        libgtk2.0-0 \
        libgtk-3-0 \
        libgbm-dev \
        libnotify-dev \
        libgconf-2-4 \
        libnss3 \
        libxss1 \
        libasound2 \
        libxtst6 \
        libxss1 \
        xauth \
        xvfb \
        unzip \
        zip \
        software-properties-common \
    && rm -rf /var/lib/apt/lists/*

# Install Java 17
ENV JAVA_HOME /usr/lib/jvm/java-17-openjdk-amd64/

RUN set -x \
    && apt-get update \
    && apt-get install --no-install-recommends -y \
        ca-certificates-java \
        openjdk-17-jre-headless \
        openjdk-17-jre \
        openjdk-17-jdk-headless \
        openjdk-17-jdk \
    && export JAVA_HOME \
    && rm -rf /var/lib/apt/lists/*

# Install maven
ENV MAVEN_VERSION 3.9.6
ENV MAVEN_HOME /usr/share/maven

RUN mkdir -p $MAVEN_HOME \
  && curl -fsSL https://dlcdn.apache.org/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz \
    | tar -xzC $MAVEN_HOME --strip-components=1 \
  && ln -s $MAVEN_HOME/bin/mvn /usr/bin/mvn

VOLUME /root/.m2

# Install node 20
RUN set -x \
    && curl -sL https://deb.nodesource.com/setup_20.x | bash - \
    && apt-get update \
    && apt-get install --no-install-recommends -y \
        nodejs \
    && npm install -g npm@latest \
    # Make 'node' available
    && touch ~/.bashrc \
    && echo 'alias nodejs=node' > ~/.bashrc \
    && rm -rf /var/lib/apt/lists/*

# Install yarn 1.22+

RUN set -x \
    && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo 'deb https://dl.yarnpkg.com/debian/ stable main' > /etc/apt/sources.list.d/yarn.list \
    && apt-get update \
    && apt-get install --no-install-recommends -y \
        yarn \
    && rm -rf /var/lib/apt/lists/* /etc/apt/sources.list.d/yarn.list

# Install Chrome
ENV CHROME_BIN /usr/bin/google-chrome

ADD scripts/xvfb-chrome /usr/bin/xvfb-chrome
RUN set -x \
    && curl -sS https://dl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && echo 'deb http://dl.google.com/linux/chrome/deb/ stable main' > /etc/apt/sources.list.d/chrome.list \
    && apt-get update \
    && apt-get install --no-install-recommends -y \
        google-chrome-stable \
    && ln -sf /usr/bin/xvfb-chrome $CHROME_BIN \
    && rm -rf /var/lib/apt/lists/* /etc/apt/sources.list.d/chrome.list

# Install firefox
ENV FIREFOX_BIN /usr/bin/firefox

ADD scripts/xvfb-firefox /usr/bin/xvfb-firefox
RUN set -x \
    && apt-get update \
    && apt-get install --no-install-recommends -y \
        firefox-esr \
    && ln -sf /usr/bin/xvfb-firefox $FIREFOX_BIN \
    && rm -rf /var/lib/apt/lists/*

# RUN node -v
# RUN npm -v
# RUN yarn -v
# RUN java -version
# RUN mvn -v
# RUN apt-cache policy firefox-esr | grep Installed | sed -e "s/Installed/Firefox/"
# RUN apt-cache policy google-chrome-stable | grep Installed | sed -e "s/Installed/Chrome/"
