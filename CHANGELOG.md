# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [2024-03-22]

### Changed

* Node v18 -> v20 because AUI will specify 20 in its `.nvmrc` (SHPLVIII-823)

## [2024-04-20]

### Changed

* Maven 3.9.1 -> 3.9.6

## [2023-04-26]

### Changed

* Maven 3.9.0 -> 3.9.1
* Java 1.8 -> 17 Old version out of maintenance and not available at
``http://security.debian.org/debian-security stretch/updates main`` anymore`

## [2023-02-14]

### Changed

* Node v16 -> v18
* Maven 3.8.6 -> 3.9.0

## [2022-12-29]

### Changed

* Remove unused sources `stretch-backports`
* Remove `mvnvm`

## [2022-12-07]

### Fixed

* Failed pipeline due to insufficient permission to push image to Docker Hub

## [2022-12-06]

### Fixed

* Upgrade to bullseye

### Changed

* Node v14 -> v16
* Add stretch-backports sources list to meet google-chrome-stable newer libgbm1 prerequisites

## [2022-11-28]

* Maven 3.8.4 -> 3.8.6 (3.8.4 has been removed from the CDN)

## [2022-02-04]

* Maven 3.8.3 -> 3.8.4

## [2021-10-27]

### Changed

* Maven 3.8.1 -> 3.8.3
* Download maven from official CDN

## [2021-08-19]

### Added

* unzip package

## [2021-05-14]

### Changed

* Node v12 -> v14
* Maven 3.5.4 -> 3.8.1
* Remove redundant dependencies from installation process
* Remove redundant layers from docker image
